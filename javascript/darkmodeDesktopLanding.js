const darkmodeToggleLanding = document.querySelector('.toggle-desktop');
const navBarLanding = document.querySelector('.navbar-desktop'); // multiple burger menus need to be select

function darkModeLanding() {
	let burgerMenuLanding = document.querySelectorAll('#stacklines');
	burgerMenuLanding.forEach((stack) => {
		stack.classList.toggle('darkmode-color-white'); // adds the class 'darkbody' to each instance in the nodelist
	});
	let navLinksLanding = navBarLanding.querySelectorAll('#nav-link');
	navLinksLanding.forEach((link) => {
		link.classList.toggle('darkmode-color-white');
	});
	navBarLanding.classList.toggle('dark-bgc');
	document.body.classList.toggle('darkbody');

	let sectionZeroImgLanding = document.querySelector('.section0');
	sectionZeroImgLanding.classList.toggle('new-bg-img');

	let sectionTwoLanding = document.querySelector('.section2');
	sectionTwoLanding.classList.toggle('darkbody');

	let clockTriggerLanding = document.querySelector('.clock');
	clockTriggerLanding.classList.toggle('darkmode-color-white');

	let sliderRightLanding = document.querySelector('#rotate');
	sliderRightLanding.classList.toggle('darkmode-color-white');
}

darkmodeToggleLanding.addEventListener('click', darkModeLanding);
