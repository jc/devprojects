const darkmodeToggleMobile = document.querySelector('.toggle-desktop');
const navBarMobile = document.querySelector('.navbar-mobile'); // multiple burger menus need to be select

function darkModeMobile() {
	let burgerMenuMobile = document.querySelectorAll('#stacklines');
	burgerMenuMobile.forEach((stack) => {
		stack.classList.toggle('darkmode-color-white'); // adds the class 'darkbody' to each instance in the nodelist
	});
	let navLinksMobile = navBar.querySelectorAll('#nav-link');
	navLinksMobile.forEach((link) => {
		link.classList.toggle('darkmode-color-white');
	});
	navBar.classList.toggle('dark-bgc');
	document.body.classList.toggle('darkbody');
	let sectionZeroImgMobile = document.querySelector('.section0');
	sectionZeroImgMobile.classList.toggle('new-bg-img');

	let sectionTwoMobile = document.querySelector('.section2');
	sectionTwoMobile.classList.toggle('darkbody');

	let clockTriggerMobile = document.querySelector('.clock');
	clockTriggerMobile.classList.toggle('darkmode-color-white');
	let sliderRightMobile = document.querySelector('#rotate');
	sliderRightMobile.classList.toggle('darkmode-color-white');
}

darkmodeToggleMobile.addEventListener('click', darkModeMobile);
