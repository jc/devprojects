const sliderButton = document.querySelector('.slider');
const slideSection = document.querySelector('.slidercontainer');
const fillButton = document.querySelector('.slide-button');

function slider() {
	slideSection.classList.toggle('open-active');

	fillButton.classList.toggle('solid');
}

sliderButton.addEventListener('click', slider);
