const darkmodeToggle = document.querySelector('.toggle-desktop');
const navBar = document.querySelector('.navbar-desktop'); // multiple burger menus need to be select

function darkMode() {
	let burgerMenu = document.querySelectorAll('#stacklines');
	burgerMenu.forEach((stack) => {
		stack.classList.toggle('darkmode-color-white'); // adds the class 'darkbody' to each instance in the nodelist
	});
	let navLinks = navBar.querySelectorAll('#nav-link');
	navLinks.forEach((link) => {
		link.classList.toggle('darkmode-color-white');
	});
	navBar.classList.toggle('dark-bgc');
	document.body.classList.toggle('darkbody');
	fillButton.classList.toggle('darkmode-color-white');

	let sectionZeroImg = document.querySelector('.section0');
	sectionZeroImg.classList.toggle('new-bg-img');

	let sectionTwo = document.querySelector('.section2');
	sectionTwo.classList.toggle('darkbody');

	let clockTrigger = document.querySelector('.clock');
	clockTrigger.classList.toggle('darkmode-color-white');

	let sliderRight = document.querySelector('#rotate');
	sliderRight.classList.toggle('darkmode-color-white');
}

darkmodeToggle.addEventListener('click', darkMode);
