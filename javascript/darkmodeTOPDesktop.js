const darkmodeToggleTopDesktop = document.querySelector('.toggle-desktop');
const navBarTopDesktop = document.querySelector('.navbar-desktop'); // multiple burger menus need to be select

function darkModeTopDesktop() {
	let burgerMenuTopDesktop = document.querySelectorAll('#stacklines');
	burgerMenuTopDesktop.forEach((stack) => {
		stack.classList.toggle('darkmode-color-white'); // adds the class 'darkbody' to each instance in the nodelist
	});
	let navLinksTopDesktop = navBarTopDesktop.querySelectorAll('#nav-link');
	navLinksTopDesktop.forEach((link) => {
		link.classList.toggle('darkmode-color-white');
	});
	navBarTopDesktop.classList.toggle('dark-bgc');
	document.body.classList.toggle('darkbody');
	fillButtonTop.classList.toggle('darkmode-color-white');

	let sectionTwoTopDesktop = document.querySelector('.section2');
	sectionTwoTopDesktop.classList.toggle('darkbody');

	let clockTriggerTopDesktop = document.querySelector('.clock');
	clockTriggerTopDesktop.classList.toggle('darkmode-color-white');

	let sliderRightTopDesktop = document.querySelector('#rotate');
	sliderRightTopDesktop.classList.toggle('darkmode-color-white');
}

darkmodeToggleTopDesktop.addEventListener('click', darkModeTopDesktop);
