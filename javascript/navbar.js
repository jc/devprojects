const logoBox = document.querySelector('.logo-box');

function expandNavigation() {
	let expandNavi = document.querySelector('.navbar-desktop');
	expandNavi.classList.add('expand-navbar');

	logoBox.classList.add('no-border');
}

logoBox.addEventListener('click', expandNavigation);
