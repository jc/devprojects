const closeNavi = document.querySelector('.close-navbar');

function contractNavigation() {
	let expandNavi = document.querySelector('.navbar-desktop');
	expandNavi.classList.remove('expand-navbar');
}

function noBorder() {
	logoBox.classList.remove('no-border');
}

closeNavi.addEventListener('click', contractNavigation);
noBorder.addEventListener('transitionend', contractNavigation);
