window.transitionToPage = function(href) {
    document.getElementById("topbody").style.opacity = 0
    setTimeout(function() {
        window.location.href = href
    }, 500)
}

document.addEventListener('DOMContentLoaded', function(event) {
    document.getElementById("topbody").style.opacity = 1
})