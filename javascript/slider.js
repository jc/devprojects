const sliderButton = document.querySelector('.slider');
const slideSection = document.querySelector('.slidercontainer');

function slider() {
	slideSection.classList.toggle('open-active');
}

sliderButton.addEventListener('click', slider);
