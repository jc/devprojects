const sliderButtonTopDesktop = document.querySelector('.slider');
const slideSectionTopDesktop = document.querySelector('.slidercontainer');
const fillButtonTop = document.querySelector('.slide-button');

function sliderTopDesktop() {
	slideSectionTopDesktop.classList.toggle('open-active');
	fillButtonTop.classList.toggle('solid');
}

sliderButtonTopDesktop.addEventListener('click', sliderTopDesktop);
