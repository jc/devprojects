const expandNaviLanding = document.querySelector('.navbar-desktop');

function expandNavigationLanding() {
	expandNaviLanding.classList.toggle('expand-navbar');

	let logoBoxLanding = document.querySelector('.logo-box');
	logoBoxLanding.classList.toggle('no-border');
}

expandNaviLanding.addEventListener('click', expandNavigationLanding);
