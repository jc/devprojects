function showTime(){
    var date = new Date(); // "captures" one moment in time (right now)
    var h = date.getHours(); // 0 - 23, captures the hours of the new Date()
    var m = date.getMinutes(); // 0 - 59, same with minutes
    var s = date.getSeconds(); // 0 - 59, an seconds
    
    h = (h < 10) ? "0" + h : h; //this is for single digit times, like 9 o'clock, to show it as 09:00:00 instead of 9:00:00
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();